﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace JacksonHewitt.Web.Models
{
	public class LocationModel
	{
		public int ID { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string CityURL
		{
			get
			{
				return City.Replace(" ", "-").Replace(".", "").Replace("/", "-").ToLower();
			}
		}
		public string State { get; set; }
		public string Zip { get; set; }
		public string Phone { get; set; }
		public string ProcessingCenter { get; set; }
		public int EntityNumber { get; set; }
		public string EntityName { get; set; }
		public string XCoordinate { get; set; }
		public string YCoordinate { get; set; }
		public bool IsStoreOpenStatus { get; set; }

	}	
}