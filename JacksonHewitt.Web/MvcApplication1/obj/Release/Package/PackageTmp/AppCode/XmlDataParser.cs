﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Caching;
using System.Xml;
using System.Xml.Linq;
using JacksonHewitt.Web.Models;

namespace MvcApplication1.App_Code
{
	public class XmlDataParser
	{
		private const string xmlCacheKey = "JHLocationDataXML";
		private const string statesCacheKey = "JHStatesList";
		private const string locationDataCacheKey = "JHLocationDataProperty";
		private const string locationCacheKey = "JHLocation{0}";
		private const string locationsByStateCacheKey = "JHLocationStates{0}";


		#region States

		public static IEnumerable<string> GetAllStates()
		{
			if (HttpContext.Current.Cache[statesCacheKey] == null)
			{
				var stateData = LocationDataList.Select(s => s.Attribute("St").Value);

				//Marshall our State list
				List<string> statesList = new List<string>();
				foreach (var s in stateData.OrderBy(s => s.Substring(0)))
				{
					if (!statesList.Contains(s.ToString()))
					{
						statesList.Add(s.ToString());
					}
				}
				HttpContext.Current.Cache.Add(statesCacheKey, statesList, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null);
				return statesList;
			}
			
			return (List<string>)HttpContext.Current.Cache[statesCacheKey];
		}

		public static List<LocationModel> GetLocationsByState(string state)
		{
			string cacheKey = string.Format(locationsByStateCacheKey, state);

			if (HttpContext.Current.Cache[cacheKey]==null)
			{
				var locationData = LocationDataList.Where(s => s.Attribute("St").Value == state).Distinct();

				List<LocationModel> locations = new List<LocationModel>();
				foreach (var l in locationData)
				{
					locations.Add(MarshallLocation(l));
				}
				HttpContext.Current.Cache.Add(cacheKey, locations, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
				return locations;
			}

			return (List<LocationModel>)HttpContext.Current.Cache[cacheKey];
		}

		#endregion

		#region Locations

		public static LocationModel GetLocationByID(int id)
		{
			string cacheString = String.Format(locationCacheKey, id);
			if (HttpContext.Current.Cache[cacheString] == null)
			{
				//XML Query
				var loc = LocationDataList.Where(l => Convert.ToInt32(l.Attribute("ID").Value) == id).FirstOrDefault();

				LocationModel location = MarshallLocation(loc);
				
				HttpContext.Current.Cache.Add(cacheString, location, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null);
				return location;
			}

			return (LocationModel)HttpContext.Current.Cache[cacheString];
		}

		#endregion

		#region LocationsData
		public static IEnumerable<XElement> LocationDataList
		{
			get
			{
				if (HttpContext.Current.Cache[locationDataCacheKey] == null)
				{
					XDocument doc = GetXmlDataFromCache();
					var locationData =
					   from item in doc.Descendants("Location")
					   select item;

					HttpContext.Current.Cache.Add(locationDataCacheKey, locationData, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
					return locationData;
				}
				return (IEnumerable<XElement>)HttpContext.Current.Cache[locationDataCacheKey];
			}
		}
		#endregion

		#region DataDocument
		private static XDocument ParseDataFile()
		{
			string filePath = String.Format("{0}\\App_Data\\JHLocations.xml", Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase));
			filePath = filePath.Replace("bin\\", "");
			string localPath = new Uri(filePath).LocalPath;
			using (StreamReader reader = File.OpenText(localPath))
			{
				XDocument doc = XDocument.Load(reader);
				return doc;
			}
		}

		public static XDocument GetXmlDataFromCache()
		{
			if (HttpContext.Current.Cache[xmlCacheKey] == null)
			{
				HttpContext.Current.Cache.Add(xmlCacheKey, ParseDataFile(), null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null);
			}
			return (XDocument)HttpContext.Current.Cache[xmlCacheKey];
		}

		#endregion

		#region Object Marshalling

		public static LocationModel MarshallLocation(XElement l)
		{
			LocationModel location = new LocationModel();
			location.ID = Convert.ToInt32(l.Attribute("ID").Value);
			location.Address = l.Attribute("Add").Value;
			location.City = l.Attribute("Cit").Value;
			location.State = l.Attribute("St").Value;
			location.Zip = l.Attribute("Zp").Value;
			location.Phone = l.Attribute("Ph").Value;
			location.XCoordinate = l.Attribute("xcoord").Value;
			location.YCoordinate = l.Attribute("ycoord").Value;

			return location;
		}

		#endregion

	}


}