﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
	public class RedirectsController : Controller
	{
		//
		// GET: /Redirects/

		public ActionResult Advance200()
		{
			return RedirectPermanent("/tax-preparation/special-offers/tax-return-advance-200");
		}
		public ActionResult Advance300()
		{
			return RedirectPermanent("/tax-preparation/special-offers/tax-return-advance-300");
		}
		public ActionResult Advance500()
		{
			return RedirectPermanent("/tax-preparation/special-offers/tax-return-advance-500");
		}

		public ActionResult ACA()
		{
			return RedirectPermanent("/tax-preparation/resource-center/healtcare-insurance-taxes");
		}



	}
}
