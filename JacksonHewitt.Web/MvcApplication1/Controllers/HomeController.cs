﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using JacksonHewitt.Web;
using System.Xml.Linq;
using MvcApplication1.App_Code;
using JacksonHewitt.Web.Models;

namespace MvcApplication1.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Locations(string state, int id, string city)
		{
			LocationModel location = XmlDataParser.GetLocationByID(id);

			// Convert the inconsistent phone number format to a number
			long num;
			if (location.Phone.Length>0)
			{
				num = Convert.ToInt64(location.Phone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", ""));
				location.Phone = String.Format("{0: (###) ###-####}", num);
			}

			if (location==null)
			{
				throw new Exception("The location specified could not be found.");
			}

			return View(location);
		}

		public ActionResult State(string state)
		{
			ViewBag.State = StatesHelper.GetStateName(state);
			ViewBag.Locations = XmlDataParser.GetLocationsByState(state);
			return View();
		}

		public ActionResult Sitemap()
		{
			ViewBag.States = XmlDataParser.GetAllStates();
			return View();
		}

		public ActionResult LocationUrls()
		{
			ViewBag.States = XmlDataParser.GetAllStates();
			return View();
		}

		#region Promotions

		public ActionResult Walmart50()
		{
			return RedirectPermanent("~/wm2");
		}
        public ActionResult Walmart75()
        {
            return View();
        }
        public ActionResult Walmart100()
        {
            return View();
        }
		public ActionResult OnlineWalmart()
		{
			return View();
		}
        public ActionResult FreeState()
        {
            return View();
        }
        public ActionResult OnlineWalmart_Alt()
		{
			return View();
		}

		public ActionResult OnlineWalmart_giftcard()
		{
			return View();
		}

		public ActionResult BrandStoryPage()
		{
			return View();
		}

		public ActionResult Visa50PrepaidCard()
		{
			return View();
		}

        public ActionResult Visa100()
        {
            return View();
        }
		public ActionResult Advance200()
		{
            return RedirectPermanent("http://www.jacksonhewitt.com");
		}
		public ActionResult Advance300()
		{
            return RedirectPermanent("http://www.jacksonhewitt.com");
        }
		public ActionResult Advance500()
		{
            return RedirectPermanent("http://www.jacksonhewitt.com");
        }

		public ActionResult JacksonHewittOnline()
		{
			return View();
		}
		public ActionResult ReferAFriend()
		{
			return View();
		}
		public ActionResult AffordableCareAct()
		{
			return View();
		}

		public ActionResult TDN()
		{
			return View();
		}

        public ActionResult TDNRedirect()
		{
            return RedirectPermanent("~/back-taxes");
		}

        public ActionResult FileYourReturn()
        {
            return View();
        }
        public ActionResult TRR()
        {
            return View();
        }


        public ActionResult thankyou2015()
        {
            return View();
        }


		public ActionResult FFA()
		{
			return View();
		}

        public ActionResult FederalExtensionFree()
        {
            return RedirectPermanent("http://www.jacksonhewittonline.com/FreeFile");
        }

        public ActionResult FederalExtensionNoPrice()
        {
            return RedirectPermanent("http://www.jacksonhewittonline.com/FreeFile");
        }

		#endregion

	}
}
