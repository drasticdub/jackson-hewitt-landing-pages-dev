﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MvcApplication1
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			// Error Page
			routes.MapRoute(
				name: "Error",
				url: "error",
				defaults: new { controller = "Error", action = "Error", id = UrlParameter.Optional }
				);


			// Locations Pages
			routes.MapRoute(
				name: "Locations Route",
				url: "tax-services-locations/{state}/{id}/tax-preparation-{city}",
				defaults: new { controller = "Home", action="Locations", id = UrlParameter.Optional}
				);



            // Federal Extension
            routes.MapRoute(
                name: "Free Federal Extension Route",
                url: "free-fileextension",
                defaults: new { controller = "Home", action = "FederalExtensionFree", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "No Price Federal Extension Route",
                url: "fileextension",
                defaults: new { controller = "Home", action = "FederalExtensionNoPrice", id = UrlParameter.Optional }
            );



			// Sitemap Pages
			routes.MapRoute(
				name: "Sitemap Route",
				url: "Sitemap",
				defaults: new { controller = "Home", action = "Sitemap", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "State Sitemap Route",
				url: "tax-services-locations/{state}",
				defaults: new { controller = "Home", action = "State", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Location URLs",
				url: "LocationURLs",
				defaults: new { controller = "Home", action = "LocationUrls", id = UrlParameter.Optional }
			);

			// Promotion Pages
			routes.MapRoute(
				name: "Walmart 50",
				url: "tax-preparation/special-offers/walmart-gift-card-50",
				defaults: new { controller = "Home", action = "Walmart50", id = UrlParameter.Optional }
			);
            routes.MapRoute(
                name: "Walmart 75",
                url: "wm75",
                defaults: new { controller = "Home", action = "Walmart75", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Walmart 100",
                url: "wm100",
                defaults: new { controller = "Home", action = "Walmart100", id = UrlParameter.Optional }
            );
			routes.MapRoute(
				name: "OnlineWalmart",
				url: "wm2",
				defaults: new { controller = "Home", action = "OnlineWalmart", id = UrlParameter.Optional }
			);
            routes.MapRoute(
                name: "FreeState",
                url: "freestate",
                defaults: new { controller = "Home", action = "FreeState", id = UrlParameter.Optional }
            ); routes.MapRoute(
				name: "wmjho",
				url: "wmjho",
				defaults: new { controller = "Home", action = "OnlineWalmart_Alt", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "wmcard",
				url: "wmcard",
				defaults: new { controller = "Home", action = "OnlineWalmart_giftcard", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Visa $50 Prepaid Card",
				url: "tax-preparation/special-offers/visa-gift-card-50",
				defaults: new { controller = "Home", action = "Visa50PrepaidCard", id = UrlParameter.Optional }
			);
            routes.MapRoute(
                name: "Visa $100 Prepaid Card",
                url: "visa100",
                defaults: new { controller = "Home", action = "Visa100", id = UrlParameter.Optional }
            );
            routes.MapRoute(
				name: "Advance $200",
				url: "tax-preparation/special-offers/tax-return-advance-200",
				defaults: new { controller = "Home", action = "Advance200", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Advance $300",
				url: "tax-preparation/special-offers/tax-return-advance-300",
				defaults: new { controller = "Home", action = "Advance300", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Advance $500",
				url: "tax-preparation/special-offers/tax-return-advance-500",
				defaults: new { controller = "Home", action = "Advance500", id = UrlParameter.Optional }
			);

			// Promo Shortcuts / Redirects
			routes.MapRoute(
				name: "Advance 200",
				url: "200",
				defaults: new { controller = "Redirects", action = "Advance200", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Advance 300",
				url: "300",
				defaults: new { controller = "Redirects", action = "Advance300", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Advance 500",
				url: "500",
				defaults: new { controller = "Redirects", action = "Advance500", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "ACA",
				url: "ACA",
				defaults: new { controller = "Redirects", action = "ACA", id = UrlParameter.Optional }
			);

            routes.MapRoute(
                name: "TDNRedirect",
                url: "tdn",
                defaults: new { controller = "Home", action = "TDNRedirect", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "TDN",
                url: "back-taxes",
                defaults: new { controller = "Home", action = "TDN", id = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "FileYourReturn",
                url: "fileyourreturn",
                defaults: new { controller = "Home", action = "FileYourReturn", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "TRR",
                url: "trr",
                defaults: new { controller = "Home", action = "TRR", id = UrlParameter.Optional }
            );
			// Informationals
			routes.MapRoute(
				name: "Jackson Hewitt Online",
				url: "tax-preparation/do-your-taxes-online",
				defaults: new { controller = "Home", action = "JacksonHewittOnline", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Refer A Friend",
				url: "tax-preparation/special-offers/refer-a-friend",
				defaults: new { controller = "Home", action = "ReferAFriend", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Affordable Care Act",
				url: "tax-preparation/resource-center/healthcare-insurance-taxes",
				defaults: new { controller = "Home", action = "AffordableCareAct", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Brand Story Page",
				url: "tax-preparation/about-jackson-hewitt",
				defaults: new { controller = "Home", action = "BrandStoryPage", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "FFA Page",
				url: "jhoffa",
				defaults: new { controller = "Home", action = "FFA", id = UrlParameter.Optional }
			);
			routes.MapRoute(
				name: "Thank You 2015",
				url: "tax-preparation/thank-you-2015",
				defaults: new { controller = "Home", action = "thankyou2015", id = UrlParameter.Optional }
			);


			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);

		}
	}
}