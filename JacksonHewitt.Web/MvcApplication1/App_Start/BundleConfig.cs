﻿using System.Web;
using System.Web.Optimization;

namespace MvcApplication1
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			
			bundles.Add(new ScriptBundle("~/bundles/js").Include(
				"~/Scripts/*.js",
				"~/Scripts/jquery.simplemodal.1.4.4.js"));

            bundles.Add(new ScriptBundle("~/bundles/js-responsive").Include(
                "~/Scripts/smoothscroll.min.js",
                "~/Scripts/backbone.js",
                "~/Scripts/responsive-accordion.min.js",
                "~/Scripts/jquery.simplemodal.1.4.4.js"));

			bundles.Add(new StyleBundle("~/bundles/css").Include(
				"~/Content/main.css", 
				"~/Content/normalize.css"));

            bundles.Add(new StyleBundle("~/bundles/css-responsive").Include(
                "~/Content/normalize.css",
                "~/Content/main-responsive.css"));
		}
	}
}