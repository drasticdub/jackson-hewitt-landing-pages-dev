Changes made:

- Head of main_template.html: js file links added
- New page: Locations.html
- New CSS (all added to main.css)
- New scripts in script directory.

Notes:
- Can ignore fonts they have remained the same.
- Let me know if I need to change font paths etc
